import { Router } from "express";
import createError from "http-errors";
import { check, validationResult, param } from "express-validator";

const router = new Router();

export default (tasks) => {
	// Set routes. I didn't create separate routers as there are only 5 routes
	router.get("/api/tasks/completed", (_, res, next) => {
		try {
			const resTasks = tasks.getCompletedTasks();

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(error);
		}
	});

	router.get("/api/tasks/incomplete", (_, res, next) => {
		try {
			const resTasks = tasks.getIncompleteTasks();

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(createError(500, "Unable to read incomplete tasks"));
		}
	});

	router.post(
		"/api/tasks/addTask",
		[
			check("description")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal length for task name is 3 letter!"),
			check("completed")
				.isBoolean()
				.withMessage("Missing todo task completed status. Must be boolean"),
		],
		(req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { description, completed } = req.body;

					// Add task for user and if not exist - it will be automatically created
					const result = tasks.addTask(description, completed);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = tasks.getIncompleteTasks();

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(400, `Task ${description} already exists!`));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to add task"));
			}
		}
	);

	router.post(
		"/api/tasks/:taskName/done",
		param("taskName").isLength({ min: 3 }).escape(),
		(req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { taskName } = req.params;

					const result = tasks.completeTaskByDescription(taskName);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = tasks.getIncompleteTasks();

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to complete task"));
			}
		}
	);

	return router;
};
