import { Router } from "express";
import createError from "http-errors";
import { check, validationResult, param } from "express-validator";

const router = new Router();

export default (userTasks) => {
	// Set routes. I didn't create separate routers as there are only 5 routes
	router.get("/api/user/:userId/tasks/completed", (req, res, next) => {
		try {
			const resTasks = userTasks.getCompletedTasks(req.params.userId);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks for this user id not found"));
			}
		} catch (error) {
			next(error);
		}
	});

	router.get("/api/user/:userId/tasks/incomplete", (req, res, next) => {
		try {
			const resTasks = userTasks.getIncompleteTasks(req.params.userId);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks for this user id not found"));
			}
		} catch (error) {
			console.error(error);
			next(createError(500, "Unable to read incomplete tasks"));
		}
	});

	router.post(
		"/api/user/:userId/tasks/addTask",
		[
			check("description")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal length for task name is 3 letter!"),
			check("completed")
				.isBoolean()
				.withMessage("Missing todo task completed status. Must be boolean"),
		],
		(req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					// Get user id
					const { userId } = req.params;
					const { description, completed } = req.body;

					// Add task for user and if not exist - it will be automatically created
					const result = userTasks.addTask(userId, description, completed);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = userTasks.getIncompleteTasks(userId);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(400, `Task ${description} already exists!`));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to add task"));
			}
		}
	);

	router.post(
		"/api/user/:userId/tasks/:taskName/done",
		param("taskName").isLength({ min: 3 }).escape(),
		(req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { userId, taskName } = req.params;

					const result = userTasks.completeTaskByDescription(userId, taskName);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = userTasks.getIncompleteTasks(userId);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to complete task"));
			}
		}
	);

	return router;
};
