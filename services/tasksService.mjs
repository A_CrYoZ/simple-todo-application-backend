import Task from "../modules/task.mjs";

class Tasks {
	// Make tasks property private, as we don't want to let manipulate it directly
	#tasks;

	constructor() {
		this.#tasks = [];
	}

	addTask(taskDescription, taskCompleted) {
		const taskFound = this.#tasks.find(
			(task) => task.description === taskDescription
		);
		if (!taskFound) {
			this.#tasks.push(new Task(taskDescription, taskCompleted));
			return true;
		}
		return false;
	}

	completeTaskByDescription(taskDescription) {
		const task = this.#tasks.find(
			(item) => item.description === taskDescription
		);

		if (task) {
			task.completed = true;
			return true;
		}

		return false;
	}

	getAllTasks() {
		return this.#tasks;
	}

	getCompletedTasks() {
		return this.#tasks
			.filter((task) => task.completed)
			.sort((a, b) => a.id - b.id);
	}

	getIncompleteTasks() {
		return this.#tasks
			.filter((task) => !task.completed)
			.sort((a, b) => a.id - b.id);
	}
}

export default Tasks;
