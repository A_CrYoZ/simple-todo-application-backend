import Task from "../modules/task.mjs";

class UserTasks {
	// Make tasks property private, as we don't want to let manipulate it directly
	#tasks;

	constructor() {
		this.#tasks = {};
	}

	addTask(userId, taskDescription, taskCompleted) {
		if (this.#tasks[userId]?.tasks) {
			const taskFound = this.#tasks[userId].tasks.find(
				(task) => task.description === taskDescription
			);
			if (!taskFound) {
				this.#tasks[userId].tasks.push(
					new Task(taskDescription, taskCompleted)
				);
				return true;
			}
			return false;
		}

		this.#tasks[userId] = {
			tasks: [new Task(taskDescription, taskCompleted)],
		};
		return true;
	}

	completeTaskByDescription(userId, taskDescription) {
		if (this.#tasks[userId]?.tasks) {
			const task = this.#tasks[userId].tasks.find(
				(item) => item.description === taskDescription
			);

			if (task) {
				task.completed = true;
				return true;
			}
		}
		return false;
	}

	getAllTasks(userId) {
		return this.#tasks[userId]?.tasks ?? null;
	}

	getCompletedTasks(userId) {
		if (!this.#tasks[userId]?.tasks) return null;

		return this.#tasks[userId].tasks
			.filter((task) => task.completed)
			.sort((a, b) => a.id - b.id);
	}

	getIncompleteTasks(userId) {
		if (!this.#tasks[userId]?.tasks) return null;

		return this.#tasks[userId].tasks
			.filter((task) => !task.completed)
			.sort((a, b) => a.id - b.id);
	}
}

export default UserTasks;
