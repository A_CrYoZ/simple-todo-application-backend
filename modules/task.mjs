class Task {
	constructor(description, completed) {
		this.id = Date.now();
		this.description = description;
		this.completed = completed;
	}

	get getDescription() {
		return this.description;
	}

	set setDescription(description) {
		this.description = description;
	}

	get getCompleted() {
		return this.completed;
	}

	set setCompleted(completed) {
		this.completed = completed;
	}
}

export default Task;
