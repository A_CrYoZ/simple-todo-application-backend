import express from "express";

import UserTasksService from "./services/userTaskService.mjs";
import userTasksRouter from "./routes/userTasksRouter.mjs";

import TasksService from "./services/tasksService.mjs";
import tasksRouter from "./routes/tasksRouter.mjs";

const userTasks = new UserTasksService();
const tasksService = new TasksService();

const app = express();
const port = 3001;

// Set body-parser
app.use(express.json());

// Set up routers
app.use(userTasksRouter(userTasks), tasksRouter(tasksService));

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
	if (err.status !== 404) {
		console.error(err);
	}

	const status = err.status ?? 500;

	res.status(status).send({ message: err.message });
});

app.listen(port, () => console.log(`API listening on port ${port}`));
